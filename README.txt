DESCRIPTION
--------------------------
This module was inspired by the CSS Injector module. This allows OG
Administrators to add custom CSS to a group. This is not a theme replacement 
but it allows OG admins to add CSS to pages within the Group with defining a 
custom theme. The CSS is added using Drupal's standard drupal_add_css() 
function and respects page caching, etc.

INSTALLATION
--------------------------
Use the following instruction to install OG CSS Injector
https://drupal.org/documentation/install/modules-themes/modules-7

CONFIGURATION
--------------------------
1. Go to /admin/config/group/permissions
2. Select Group Type
3. Assign 'Administer CSS Injection for Group' permission to trusted roles.** 

USING
--------------------------
The link 'Custom CSS' is added to the group tab for individuals with the 
'Administer CSS Injection for Group' permission.  Add valid CSS here and click 
save.  

Custom CSS will be loaded for all group pages (based on the current OG 
Context).


**Please note: Creative individuals can use CSS to execute code on a client's 
browser, by injecting JS snippets in place of images and so on. Treat the 
'Administer CSS Injector' permission carefully, as you would any other 
sensitive administrative tool; allowing untrusted individuals to use it is a 
security risk.
